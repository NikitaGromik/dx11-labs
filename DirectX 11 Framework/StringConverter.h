#pragma once
#include "pch.h"

namespace D3D11Framework {
	class D3D11F_API StringConverter {
	public:
		static std::wstring StringToWide(std::string str);
	};
}
