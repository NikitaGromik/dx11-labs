#include "pch.h"
#include "ModelFactory.h"
#include <DirectXMath.h>

namespace D3D11Framework {

	ModelRaw ModelFactory::GetBox(float w, float h, float d) {

		float w2 = 0.5f * w;
		float h2 = 0.5f * h;
		float d2 = 0.5f * d;

		//Textured Square
		Vertex v[] =
		{
			Vertex(-w2, -h2, -d2, 0.0f, 1.0f, 0), //FRONT Bottom Left   - [0]
			Vertex(-w2, +h2, -d2, 0.0f, 0.0f, 0), //FRONT Top Left      - [1]
			Vertex(+w2, +h2, -d2, 1.0f, 0.0f, 0), //FRONT Top Right     - [2]
			Vertex(+w2, -h2, -d2, 1.0f, 1.0f, 0), //FRONT Bottom Right   - [3]
			Vertex(-w2, -h2, +d2, 0.0f, 1.0f, 0), //BACK Bottom Left   - [4]
			Vertex(-w2, +h2, +d2, 0.0f, 0.0f, 0), //BACK Top Left      - [5]
			Vertex(+w2, +h2, +d2, 1.0f, 0.0f, 0), //BACK Top Right     - [6]
			Vertex(+w2, -h2, +d2, 1.0f, 1.0f, 0), //BACK Bottom Right   - [7]
		};


		DWORD indices[] =
		{
			0, 1, 2, //FRONT
			0, 2, 3, //FRONT
			4, 7, 6, //BACK 
			4, 6, 5, //BACK
			3, 2, 6, //RIGHT SIDE
			3, 6, 7, //RIGHT SIDE
			4, 5, 1, //LEFT SIDE
			4, 1, 0, //LEFT SIDE
			1, 5, 6, //TOP
			1, 6, 2, //TOP
			0, 3, 7, //BOTTOM
			0, 7, 4, //BOTTOM
		};

		return ModelRaw(v, indices, ARRAYSIZE(v), ARRAYSIZE(indices));
	}

	ModelRaw ModelFactory::GetSphere(float fRadius, UINT uSlices, UINT uStacks) {
        UINT cFaces = 2 * (uStacks - 1) * uSlices;
        UINT cVertices = (uStacks - 1) * uSlices + 2;

        Vertex* vertices = new Vertex[cVertices];


        UINT i, j;

        const int CACHE_SIZE = 240 * 2;

        // Sin/Cos caches
        float sinI[CACHE_SIZE], cosI[CACHE_SIZE];
        float sinJ[CACHE_SIZE], cosJ[CACHE_SIZE];

        for (i = 0; i < uSlices; i++) {
            sinI[i] = sinf((float) (2.0f * DirectX::XM_PI * i / uSlices));
            cosI[i] = cosf((float) (2.0f * DirectX::XM_PI * i / uSlices));
            // sincosf( 2.0f * D3DX_PI * i / uSlices, sinI + i, cosI + i );
        }
        for (j = 0; j < uStacks; j++) {
            sinJ[j] = sinf((float) (DirectX::XM_PI * j / uStacks));
            cosJ[j] = cosf((float) (DirectX::XM_PI * j / uStacks));
            // sincosf( D3DX_PI * j / uStacks, sinJ + j, cosJ + j );
        }

        // Generate vertices
        Vertex* pVertex = vertices;

        // +Z pole
        pVertex->_Position = XMFLOAT3(0.0f, 0.0f, fRadius);
        pVertex->_Color = XMFLOAT3(.75f, 0.75f, .15f);
        //pVertex->normal = XMFLOAT3(0.0f, 0.0f, 1.0f);
        pVertex++;

        // Stacks
        for (j = 1; j < uStacks; j++)     {
            for (i = 0; i < uSlices; i++)         {
                XMFLOAT3 norm(sinI[i] * sinJ[j], cosI[i] * sinJ[j], cosJ[j]);
                XMFLOAT3 _Pos;
                _Pos.x = norm.x * fRadius;
                _Pos.y = norm.y * fRadius;
                _Pos.z = norm.z * fRadius;

                pVertex->_Position = _Pos; //norm * fRadius;
                pVertex->_Color = XMFLOAT3(.75f + _Pos.x, 0.75f + _Pos.y, .15f + _Pos.z);
                //pVertex->normal = norm;

                pVertex++;
            }
        }

        // Z- pole
        pVertex->_Position = XMFLOAT3(0.0f, 0.0f, -fRadius);
        pVertex->_Color = XMFLOAT3(.75f, 0.75f, .15f);
        //pVertex->normal = XMFLOAT3(0.0f, 0.0f, -1.0f);
        pVertex++;


        DWORD* indices = new DWORD[cFaces * 3];

        // Generate indices
        DWORD* pwFace = indices;

        // Z+ pole
        UINT uRowA = 0;
        UINT uRowB = 1;

        for (i = 0; i < uSlices - 1; i++)     {
            pwFace[0] = (WORD) (uRowA);
            pwFace[1] = (WORD) (uRowB + i + 1);
            pwFace[2] = (WORD) (uRowB + i);
            pwFace += 3;
        }

        pwFace[0] = (WORD) (uRowA);
        pwFace[1] = (WORD) (uRowB);
        pwFace[2] = (WORD) (uRowB + i);
        pwFace += 3;

        // Interior stacks
        for (j = 1; j < uStacks - 1; j++)     {
            uRowA = 1 + (j - 1) * uSlices;
            uRowB = uRowA + uSlices;

            for (i = 0; i < uSlices - 1; i++)         {
                pwFace[0] = (WORD) (uRowA + i);
                pwFace[1] = (WORD) (uRowA + i + 1);
                pwFace[2] = (WORD) (uRowB + i);
                pwFace += 3;

                pwFace[0] = (WORD) (uRowA + i + 1);
                pwFace[1] = (WORD) (uRowB + i + 1);
                pwFace[2] = (WORD) (uRowB + i);
                pwFace += 3;
            }

            pwFace[0] = (WORD) (uRowA + i);
            pwFace[1] = (WORD) (uRowA);
            pwFace[2] = (WORD) (uRowB + i);
            pwFace += 3;

            pwFace[0] = (WORD) (uRowA);
            pwFace[1] = (WORD) (uRowB);
            pwFace[2] = (WORD) (uRowB + i);
            pwFace += 3;
        }

        // Z- pole
        uRowA = 1 + (uStacks - 2) * uSlices;
        uRowB = uRowA + uSlices;

        for (i = 0; i < uSlices - 1; i++)     {
            pwFace[0] = (WORD) (uRowA + i);
            pwFace[1] = (WORD) (uRowA + i + 1);
            pwFace[2] = (WORD) (uRowB);
            pwFace += 3;
        }

        pwFace[0] = (WORD) (uRowA + i);
        pwFace[1] = (WORD) (uRowA);
        pwFace[2] = (WORD) (uRowB);
        pwFace += 3;

		return ModelRaw(vertices, indices, cVertices, cFaces * 3);
	}

}
