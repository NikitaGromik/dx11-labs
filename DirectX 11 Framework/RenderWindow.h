#pragma once

#include "pch.h"
#include "AlertUtils.h"

namespace D3D11Framework {

	class WindowContainer;

	class D3D11F_API RenderWindow {
	public:
		bool Initialize(WindowContainer* pWindowContainer, HINSTANCE hInstance, std::string windowTitle, std::string windowClass, int width, int height);
		bool ProcessMessages();
		~RenderWindow();
		HWND GetHWND() const { return this->_Handle; };
	private:
		void RegisterWindowClass();

		/**
		 * Handle to this window.
		 */
		HWND _Handle = NULL;

		/**
		 * Handle to application instance.
		 */
		HINSTANCE _Instance = NULL;
		std::string _WindowTitle = "";
		std::wstring _WindowTitleWide = L"";
		std::string _WindowClass = "";
		std::wstring _WindowClassWide = L"";
		int _Width = 0;
		int _Height = 0;
	};
}