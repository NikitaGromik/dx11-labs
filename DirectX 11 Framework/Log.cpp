#include "pch.h"
#include "Log.h"

#define LOG_FILENAME "log.txt"

namespace D3D11Framework {

	Log* Log::_Instance = nullptr;

	Log::Log() {
		if (!_Instance) {
			_File = nullptr;
			_Instance = this;
			Init();
		}
	}
	Log::~Log() {
		Close();
		_Instance = nullptr;
	}

	void Log::P(const char* message, ...) {
		va_list args;
		va_start(args, message);

		// ���������� ����� ��������, ������� ����� �������, ���� ������, �� ������� ��������� ������ ����������, 
		// ���� ����������� ��� ���������� � ���� ��� ����� � �������������� ��������� ����� ��������������. 
		// ������������ �������� �� �������� ����������� ����-������.
		int len = _vscprintf(message, args) + 1;
		char* _Buffer = static_cast<char*>(malloc(len * sizeof(char)));

		// ���������� ����� ���������� ��������, �� ������� ����������� ����-������, 
		// ��� ������������� ��������, ���� ��������� ������ ������.
		vsprintf_s(_Buffer, len, message, args);

		_Instance->PrintToFile("", _Buffer);
		va_end(args);
		free(_Buffer);
	}

	void Log::D(const char* message, ...) {
#ifdef _DEBUG
		va_list args;
		va_start(args, message);
		int len = _vscprintf(message, args) + 1;
		char* _Buffer = static_cast<char*>(malloc(len * sizeof(char)));
		vsprintf_s(_Buffer, len, message, args);

		_Instance->PrintToFile("-- DEBUG -- ", _Buffer);
		va_end(args);
		free(_Buffer);
#endif
	}

	void Log::E(const char* message, ...) {
		va_list args;
		va_start(args, message);
		int len = _vscprintf(message, args) + 1;
		char* _Buffer = static_cast<char*>(malloc(len * sizeof(char)));
		vsprintf_s(_Buffer, len, message, args);

		_Instance->PrintToFile("-- ERROR -- ", _Buffer);
		va_end(args);
		free(_Buffer);
	}

	void Log::OnCreate(){
		_Instance = new Log();
	}

	void Log::Init() {
		setlocale(LC_ALL, "rus");

		if (fopen_s(&_File, LOG_FILENAME, "w") == 0) {
			char timer[9];
			_strtime_s(timer, 9);
			char date[9];
			_strdate_s(date, 9);
			fprintf(_File, "��� ������: %s %s.\n", date, timer);
			fprintf(_File, "---------------------------------------\n\n");
		} 		else {
			printf("������ ��� �������� ����� ����...\n");
			_File = nullptr;
		}
	}

	void Log::Close() {
		if (!_File)
			return;

		char timer[9];
		_strtime_s(timer, 9);
		char date[9];
		_strdate_s(date, 9);
		fprintf(_File, "\n---------------------------------------\n");
		fprintf(_File, "����� ����: %s %s.", date, timer);
		fclose(_File);
	}

	void Log::PrintToFile(const char* level, const char* text) {
		char timer[9];
		_strtime_s(timer, 9);
		clock_t cl = clock();

		printf("%s::%d: %s%s\n", timer, cl, level, text);
		if (_File) {
			fprintf(_File, "%s::%d: %s%s\n", timer, cl, level, text);
			fflush(_File);
		}
	}

}