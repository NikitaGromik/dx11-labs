#include "pch.h"
#include "WindowContainer.h"


namespace D3D11Framework {
	bool RenderWindow::Initialize(WindowContainer* pWindowContainer, HINSTANCE hInstance, std::string windowTitle, std::string windowClass, int width, int height) {
		this->_Instance = hInstance;
		this->_Width = width;
		this->_Height = height;
		this->_WindowTitle = windowTitle;
		this->_WindowTitleWide = StringConverter::StringToWide(this->_WindowTitle);
		this->_WindowClass = windowClass;
		this->_WindowClassWide = StringConverter::StringToWide(this->_WindowClass); //wide string representation of class string (used for registering class and creating window)

		this->RegisterWindowClass();

		this->_Handle = CreateWindowEx(0, //Extended Windows style - we are using the default. For other options, see: https://msdn.microsoft.com/en-us/library/windows/desktop/ff700543(v=vs.85).aspx
			this->_WindowClassWide.c_str(), //Window class name
			this->_WindowTitleWide.c_str(), //Window Title
			WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU, //Windows style - See: https://msdn.microsoft.com/en-us/library/windows/desktop/ms632600(v=vs.85).aspx
			200, //Window X Position
			200, //Window Y Position
			this->_Width, //Window Width
			this->_Height, //Window Height
			NULL, //Handle to parent of this window. Since this is the first window, it has no parent window.
			NULL, //Handle to menu or child window identifier. Can be set to NULL and use menu in WindowClassEx if a menu is desired to be used.
			this->_Instance, //Handle to the instance of module to be used with this window
			pWindowContainer); //Param to create window

		if (this->_Handle == NULL) {
			AlertUtils::E(GetLastError(), "CreateWindowEX Failed for window: " + this->_WindowTitle);
			return false;
		}

		// Bring the window up on the screen and set it as main focus.
		ShowWindow(this->_Handle, SW_SHOW);
		SetForegroundWindow(this->_Handle);
		SetFocus(this->_Handle);

		return true;
	}

	bool RenderWindow::ProcessMessages() {
		// Handle the windows messages.
		MSG msg;
		ZeroMemory(&msg, sizeof(MSG)); // Initialize the message structure.

		while (PeekMessage(&msg, //Where to store message (if one exists) See: https://msdn.microsoft.com/en-us/library/windows/desktop/ms644943(v=vs.85).aspx
			this->_Handle, //Handle to window we are checking messages for
			0,    //Minimum Filter Msg Value - We are not filtering for specific messages, but the min/max could be used to filter only mouse messages for example.
			0,    //Maximum Filter Msg Value
			PM_REMOVE))//Remove message after capturing it via PeekMessage. For more argument options, see: https://msdn.microsoft.com/en-us/library/windows/desktop/ms644943(v=vs.85).aspx
		{
			TranslateMessage(&msg); //Translate message from virtual key messages into character messages so we can dispatch the message. See: https://msdn.microsoft.com/en-us/library/windows/desktop/ms644955(v=vs.85).aspx
			DispatchMessage(&msg); //Dispatch message to our Window Proc for this window. See: https://msdn.microsoft.com/en-us/library/windows/desktop/ms644934(v=vs.85).aspx
		}

		// Check if the window was closed
		if (msg.message == WM_NULL) {
			if (!IsWindow(this->_Handle)) {
				this->_Handle = NULL; //Message processing loop takes care of destroying this window
				UnregisterClass(this->_WindowClassWide.c_str(), this->_Instance);
				return false;
			}
		}

		return true;
	}

	RenderWindow::~RenderWindow() {
		if (this->_Handle != NULL) {
			UnregisterClass(this->_WindowClassWide.c_str(), this->_Instance);
			DestroyWindow(_Handle);
		}
	}

	LRESULT CALLBACK HandleMsgRedirect(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
		switch (uMsg) 	{
			// All other messages
			case WM_CLOSE:
				DestroyWindow(hwnd);
				return 0;

			default:
			{
				// retrieve ptr to window class
				WindowContainer* const pWindow = reinterpret_cast<WindowContainer*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));
				// forward message to window class handler
				return pWindow->WindowProc(hwnd, uMsg, wParam, lParam);
			}
		}
	}

	LRESULT CALLBACK HandleMessageSetup(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
		switch (uMsg) 	{
			case WM_NCCREATE:
			{
				const CREATESTRUCTW* const pCreate = reinterpret_cast<CREATESTRUCTW*>(lParam);
				WindowContainer* pWindow = reinterpret_cast<WindowContainer*>(pCreate->lpCreateParams);
				if (pWindow == nullptr) //Sanity check
				{
					AlertUtils::E("Critical Error: Pointer to window container is null during WM_NCCREATE.");
					exit(-1);
				}
				SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pWindow));
				SetWindowLongPtr(hwnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(HandleMsgRedirect));
				return pWindow->WindowProc(hwnd, uMsg, wParam, lParam);
			}
			default:
				return DefWindowProc(hwnd, uMsg, wParam, lParam);
		}
	}

	void RenderWindow::RegisterWindowClass() {
		WNDCLASSEX wc; //Our Window Class (This has to be filled before our window can be created) See: https://msdn.microsoft.com/en-us/library/windows/desktop/ms633577(v=vs.85).aspx
		wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //Flags [Redraw on width/height change from resize/movement] See: https://msdn.microsoft.com/en-us/library/windows/desktop/ff729176(v=vs.85).aspx
		wc.lpfnWndProc = HandleMessageSetup; //Pointer to Window Proc function for handling messages from this window
		wc.cbClsExtra = 0; //# of extra bytes to allocate following the window-class structure. We are not currently using this.
		wc.cbWndExtra = 0; //# of extra bytes to allocate following the window instance. We are not currently using this.
		wc.hInstance = this->_Instance; //Handle to the instance that contains the Window Procedure
		wc.hIcon = NULL;   //Handle to the class icon. Must be a handle to an icon resource. We are not currently assigning an icon, so this is null.
		wc.hIconSm = NULL; //Handle to small icon for this class. We are not currently assigning an icon, so this is null.
		wc.hCursor = LoadCursor(NULL, IDC_ARROW); //Default Cursor - If we leave this null, we have to explicitly set the cursor's shape each time it enters the window.
		wc.hbrBackground = NULL; //Handle to the class background brush for the window's background color - we will leave this blank for now and later set this to black. For stock brushes, see: https://msdn.microsoft.com/en-us/library/windows/desktop/dd144925(v=vs.85).aspx
		wc.lpszMenuName = NULL; //Pointer to a null terminated character string for the menu. We are not using a menu yet, so this will be NULL.
		wc.lpszClassName = this->_WindowClassWide.c_str(); //Pointer to null terminated string of our class name for this window.
		wc.cbSize = sizeof(WNDCLASSEX); //Need to fill in the size of our struct for cbSize
		RegisterClassEx(&wc); // Register the class so that it is usable.
	}
}