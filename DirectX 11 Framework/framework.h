﻿#pragma once

#include <clocale>
#include <ctime>

#include <string>
#include <list>

#include <vector> 

#define WIN32_LEAN_AND_MEAN
// Файлы заголовков Windows
#include <windows.h>

#include <d3d11.h>

#pragma comment(lib, "d3d11.lib")

#include "StringConverter.h"