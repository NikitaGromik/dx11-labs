#pragma once

#include "MouseEvent.h"
#include <queue>

namespace D3D11Framework {

	class D3D11F_API Mouse {
	public:
		void OnLeftPressed(int x, int y);
		void OnLeftReleased(int x, int y);
		void OnRightPressed(int x, int y);
		void OnRightReleased(int x, int y);
		void OnMiddlePressed(int x, int y);
		void OnMiddleReleased(int x, int y);
		void OnWheelUp(int x, int y);
		void OnWheelDown(int x, int y);
		void OnMouseMove(int x, int y);
		void OnMouseMoveRaw(int x, int y);

		bool IsLeftDown();
		bool IsMiddleDown();
		bool IsRightDown();

		int GetPosX();
		int GetPosY();
		MousePoint GetPos();

		bool EventBufferIsEmpty();
		MouseEvent ReadEvent();

	private:
		std::queue<MouseEvent> _EventBuffer;
		bool _IsLeftDown = false;
		bool _IsRightDown = false;
		bool _IsMiddleButtonDown = false;
		int _X = 0;
		int _Y = 0;
	};
}
