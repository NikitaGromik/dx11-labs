#include "..\\pch.h"
#include "Mouse.h"

namespace D3D11Framework {

	void Mouse::OnLeftPressed(int x, int y) {
		this->_IsLeftDown = true;
		MouseEvent me(MouseEvent::EventType::LPress, x, y);
		this->_EventBuffer.push(me);
	}

	void Mouse::OnLeftReleased(int x, int y) {
		this->_IsLeftDown = false;
		this->_EventBuffer.push(MouseEvent(MouseEvent::EventType::LRelease, x, y));
	}

	void Mouse::OnRightPressed(int x, int y) {
		this->_IsRightDown = true;
		this->_EventBuffer.push(MouseEvent(MouseEvent::EventType::RPress, x, y));
	}

	void Mouse::OnRightReleased(int x, int y) {
		this->_IsRightDown = false;
		this->_EventBuffer.push(MouseEvent(MouseEvent::EventType::RRelease, x, y));
	}

	void Mouse::OnMiddlePressed(int x, int y) {
		this->_IsMiddleButtonDown = true;
		this->_EventBuffer.push(MouseEvent(MouseEvent::EventType::MPress, x, y));
	}

	void Mouse::OnMiddleReleased(int x, int y) {
		this->_IsMiddleButtonDown = false;
		this->_EventBuffer.push(MouseEvent(MouseEvent::EventType::MRelease, x, y));
	}

	void Mouse::OnWheelUp(int x, int y) {
		this->_EventBuffer.push(MouseEvent(MouseEvent::EventType::WheelUp, x, y));
	}

	void Mouse::OnWheelDown(int x, int y) {
		this->_EventBuffer.push(MouseEvent(MouseEvent::EventType::WheelDown, x, y));
	}

	void Mouse::OnMouseMove(int x, int y) {
		this->_X = x;
		this->_Y = y;
		this->_EventBuffer.push(MouseEvent(MouseEvent::EventType::Move, x, y));
	}

	void Mouse::OnMouseMoveRaw(int x, int y) {
		this->_EventBuffer.push(MouseEvent(MouseEvent::EventType::RAW_MOVE, x, y));
	}

	bool Mouse::IsLeftDown() {
		return this->_IsLeftDown;
	}

	bool Mouse::IsMiddleDown() {
		return this->_IsMiddleButtonDown;
	}

	bool Mouse::IsRightDown() {
		return this->_IsRightDown;
	}

	int Mouse::GetPosX() {
		return this->_X;
	}

	int Mouse::GetPosY() {
		return this->_Y;
	}

	MousePoint Mouse::GetPos() {
		return{this->_X, this->_Y};
	}

	bool Mouse::EventBufferIsEmpty() {
		return this->_EventBuffer.empty();
	}

	MouseEvent Mouse::ReadEvent() {
		if (this->_EventBuffer.empty()) {
			return MouseEvent();
		} else {
			MouseEvent e = this->_EventBuffer.front(); // Get first event from buffer
			this->_EventBuffer.pop(); // Remove first event from buffer
			return e;
		}
	}
}
