#pragma once

#include "..\\pch.h"

namespace D3D11Framework {

	struct D3D11F_API MousePoint {
		int x;
		int y;
	};

	class D3D11F_API MouseEvent {
	public:
		enum D3D11F_API EventType {
			LPress,
			LRelease,
			RPress,
			RRelease,
			MPress,
			MRelease,
			WheelUp,
			WheelDown,
			Move,
			RAW_MOVE,
			Invalid
		};
	private:
		EventType type;
		int x;
		int y;
	public:
		MouseEvent();
		MouseEvent(const EventType type, const int x, const int y);
		bool IsValid() const;
		EventType GetType() const;
		MousePoint GetPos() const;
		int GetPosX() const;
		int GetPosY() const;
	};
}
