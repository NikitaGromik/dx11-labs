#pragma once

#include "KeyboardEvent.h"
#include <queue>

namespace D3D11Framework {
	class D3D11F_API Keyboard {
	public:
		Keyboard();
		bool KeyIsPressed(const unsigned char keycode);
		bool KeyBufferIsEmpty();
		bool CharBufferIsEmpty();
		KeyboardEvent ReadKey();
		unsigned char ReadChar();
		void OnKeyPressed(const unsigned char key);
		void OnKeyReleased(const unsigned char key);
		void OnChar(const unsigned char key);
		void EnableAutoRepeatKeys();
		void DisableAutoRepeatKeys();
		void EnableAutoRepeatChars();
		void DisableAutoRepeatChars();
		bool IsKeysAutoRepeat();
		bool IsCharsAutoRepeat();
	private:
		bool _IsAutoRepeatKeys = false;
		bool _IsAutoRepeatChars = false;
		bool _KeyStates[256];
		std::queue<KeyboardEvent> _KeyBuffer;
		std::queue<unsigned char> _CharBuffer;
	};
}
