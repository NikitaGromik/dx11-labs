#include "..\\pch.h"
#include "Keyboard.h"

namespace D3D11Framework {
	Keyboard::Keyboard() {
		for (int i = 0; i < 256; i++)
			this->_KeyStates[i] = false; //Initialize all key states to off (false)
	}

	bool Keyboard::KeyIsPressed(const unsigned char keycode) {
		return this->_KeyStates[keycode];
	}

	bool Keyboard::KeyBufferIsEmpty() {
		return this->_KeyBuffer.empty();
	}

	bool Keyboard::CharBufferIsEmpty() {
		return this->_CharBuffer.empty();
	}

	KeyboardEvent Keyboard::ReadKey() {
		if (this->_KeyBuffer.empty()) //If no keys to be read?
		{
			return KeyboardEvent(); //return empty keyboard event
		} else {
			KeyboardEvent e = this->_KeyBuffer.front(); //Get first Keyboard Event from queue
			this->_KeyBuffer.pop(); //Remove first item from queue
			return e; //Returns keyboard event
		}
	}

	unsigned char Keyboard::ReadChar() {
		if (this->_CharBuffer.empty()) //If no keys to be read?
		{
			return 0u; //return 0 (NULL char)
		} else {
			unsigned char e = this->_CharBuffer.front(); //Get first char from queue
			this->_CharBuffer.pop(); //Remove first char from queue
			return e; //Returns char
		}
	}

	void Keyboard::OnKeyPressed(const unsigned char key) {
#if _DEBUG
		std::string w = "Pressed: ";
		w += key;
		w += "\n";
		OutputDebugStringA(w.c_str());
#endif
		this->_KeyStates[key] = true;
		this->_KeyBuffer.push(KeyboardEvent(KeyboardEvent::EventType::Press, key));
	}

	void Keyboard::OnKeyReleased(const unsigned char key) {
#if _DEBUG
		std::string w = "Released: ";
		w += key;
		w += "\n";
		OutputDebugStringA(w.c_str());
#endif
		this->_KeyStates[key] = false;
		this->_KeyBuffer.push(KeyboardEvent(KeyboardEvent::EventType::Release, key));
	}

	void Keyboard::OnChar(const unsigned char key) {
		this->_CharBuffer.push(key);
	}

	void Keyboard::EnableAutoRepeatKeys() {
		this->_IsAutoRepeatKeys = true;
	}

	void Keyboard::DisableAutoRepeatKeys() {
		this->_IsAutoRepeatKeys = false;
	}

	void Keyboard::EnableAutoRepeatChars() {
		this->_IsAutoRepeatChars = true;
	}

	void Keyboard::DisableAutoRepeatChars() {
		this->_IsAutoRepeatChars = false;
	}

	bool Keyboard::IsKeysAutoRepeat() {
		return this->_IsAutoRepeatKeys;
	}

	bool Keyboard::IsCharsAutoRepeat() {
		return this->_IsAutoRepeatChars;
	}
}