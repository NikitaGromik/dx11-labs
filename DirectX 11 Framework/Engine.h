#pragma once

#include "pch.h"
#include "WindowContainer.h"

namespace D3D11Framework {
	class D3D11F_API Engine : WindowContainer {
	public:
		bool Initialize(HINSTANCE hInstance, std::string windowTitle, std::string windowClass, int width, int height);
		bool ProcessMessages();
		void Update();
		void Render();

		void AddModel(Model* m) {
			this->_GFX.AddModel(m);
		}

		bool InitializeModels() {
			return this->_GFX.InitializeModels();
		}

		Keyboard GetKeyboard() const {
			return _Keyboard;
		}
	};
}
