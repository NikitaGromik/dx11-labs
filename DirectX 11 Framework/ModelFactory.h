#pragma once

#include "pch.h"
#include "Graphics/Model.h"

namespace D3D11Framework {
	class D3D11F_API ModelFactory {
	public:
		static ModelRaw GetBox(float w, float h, float d);

		static ModelRaw GetSphere(float fRadius, UINT uSlices, UINT uStacks);
	};
}