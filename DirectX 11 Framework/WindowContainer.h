#pragma once

#include "pch.h"
#include "RenderWindow.h"
#include "Keyboard/Keyboard.h"
#include "Mouse/Mouse.h"
#include "Graphics/Graphics.h"

#include <memory>

namespace D3D11Framework {
	class D3D11F_API WindowContainer {
	public:
		WindowContainer();

		LRESULT WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	protected:
		RenderWindow _RenderWindow;

		Keyboard _Keyboard;
		Mouse _Mouse;
		Graphics _GFX;
	};
}
