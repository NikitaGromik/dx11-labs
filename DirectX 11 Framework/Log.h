#pragma once

#include "pch.h"

namespace D3D11Framework {
	class D3D11F_API Log {
	public:
		Log();
		~Log();

		// Print.
		static void P(const char* message, ...);

		// Debug.
		static void D(const char* message, ...);

		// Error.
		static void E(const char* message, ...);

		static void OnCreate();

	private:
		static Log* _Instance;

		FILE* _File;

		void Init();
		void Close();
		void PrintToFile(const char* level, const char* text);
	};
}

