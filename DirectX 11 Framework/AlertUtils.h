#pragma once
#include "StringConverter.h"
#include <Windows.h>

namespace D3D11Framework {
	class D3D11F_API AlertUtils {
	public:
		// Error.
		static void E(std::string message);
		static void E(HRESULT hr, std::string message);
		static void E(HRESULT hr, std::wstring message);
	};
}
