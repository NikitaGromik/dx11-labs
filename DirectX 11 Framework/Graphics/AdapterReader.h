#pragma once
#include "..\\AlertUtils.h"
#pragma comment(lib,"DXGI.lib")

namespace D3D11Framework {

	class D3D11F_API AdapterData {
	public:
		AdapterData(IDXGIAdapter* pAdapter);
		IDXGIAdapter* pAdapter = nullptr;
		DXGI_ADAPTER_DESC description;
	};

	class D3D11F_API AdapterReader {
	public:
		static std::vector<AdapterData> GetAdapters();
	private:
		static std::vector<AdapterData> adapters;
	};
}
