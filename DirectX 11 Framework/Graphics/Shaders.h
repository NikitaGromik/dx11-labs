#pragma once

#include "..\\pch.h"
#include "..\\AlertUtils.h"

#pragma comment(lib, "D3DCompiler.lib")
#include <d3dcompiler.h>

namespace D3D11Framework {
	class D3D11F_API VertexShader {
	public:
		bool Initialize(ID3D11Device* _Device, std::wstring shaderpath, D3D11_INPUT_ELEMENT_DESC* layoutDesc, UINT count);
		ID3D11VertexShader* GetShader();
		ID3D10Blob* GetBuffer();
		ID3D11InputLayout* GetInputLayout();
	private:
		ID3D11VertexShader* _Shader;
		ID3D10Blob* _ShaderBuffer;
		ID3D11InputLayout* _InputLayout;
	};

	class D3D11F_API PixelShader {
	public:
		bool Initialize(ID3D11Device* _Device, std::wstring shaderpath);
		ID3D11PixelShader* GetShader();
		ID3D10Blob* GetBuffer();
	private:
		ID3D11PixelShader* _Shader;
		ID3D10Blob* _ShaderBuffer;
	};
}
