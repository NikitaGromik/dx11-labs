#include "..\\pch.h"
#include "Shaders.h"

namespace D3D11Framework {

	bool VertexShader::Initialize(ID3D11Device* _Device, std::wstring shaderpath, D3D11_INPUT_ELEMENT_DESC* layoutDesc, UINT count) {
		HRESULT hr = D3DReadFileToBlob(shaderpath.c_str(), &this->_ShaderBuffer);
		if (FAILED(hr)) {
			std::wstring errorMsg = L"Failed to load shader: ";
			errorMsg += shaderpath;
			AlertUtils::E(hr, errorMsg);
			return false;
		}

		hr = _Device->CreateVertexShader(this->_ShaderBuffer->GetBufferPointer(), this->_ShaderBuffer->GetBufferSize(), NULL, &this->_Shader);
		if (FAILED(hr)) {
			std::wstring errorMsg = L"Failed to create vertex shader: ";
			errorMsg += shaderpath;
			AlertUtils::E(hr, errorMsg);
			return false;
		}

		hr = _Device->CreateInputLayout(layoutDesc, count,
			this->_ShaderBuffer->GetBufferPointer(),
			this->_ShaderBuffer->GetBufferSize(),
			&this->_InputLayout
		);

		if (FAILED(hr)) {
			AlertUtils::E(hr, "Failed to create input layout.");
			return false;
		}

		return true;
	}

	ID3D11VertexShader* VertexShader::GetShader() {
		return this->_Shader;
	}

	ID3D10Blob* VertexShader::GetBuffer() {
		return this->_ShaderBuffer;
	}
	ID3D11InputLayout* VertexShader::GetInputLayout() {
		return this->_InputLayout;
	}

	// ------------------------------------------------------------------------
	// Pixel shader.
	// ------------------------------------------------------------------------

	bool PixelShader::Initialize(ID3D11Device* _Device, std::wstring shaderpath) {
		HRESULT hr = D3DReadFileToBlob(shaderpath.c_str(), &this->_ShaderBuffer);
		if (FAILED(hr)) {
			std::wstring errorMsg = L"Failed to load shader: ";
			errorMsg += shaderpath;
			AlertUtils::E(hr, errorMsg);
			return false;
		}

		hr = _Device->CreatePixelShader(
			this->_ShaderBuffer->GetBufferPointer(),
			this->_ShaderBuffer->GetBufferSize(),
			NULL,
			&this->_Shader
		);
		if (FAILED(hr)) {
			std::wstring errorMsg = L"Failed to create pixel shader: ";
			errorMsg += shaderpath;
			AlertUtils::E(hr, errorMsg);
			return false;
		}

		return true;
	}

	ID3D11PixelShader* PixelShader::GetShader() {
		return this->_Shader;
	}

	ID3D10Blob* PixelShader::GetBuffer() {
		return this->_ShaderBuffer;
	}

}
