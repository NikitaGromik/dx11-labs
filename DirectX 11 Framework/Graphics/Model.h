#pragma once

#include "..\\pch.h"

#include "Vertex.h"
#include "IndexBuffer.h"
#include "ConstantBuffer.h"
#include "ConstantBufferType.h"
#include "VertexBuffer.h"

using namespace DirectX;

namespace D3D11Framework {

	class D3D11F_API ModelRaw {
	public:
		std::vector<Vertex> _Vertices;
		std::vector<DWORD> _Indices;

		ModelRaw() {
			_Vertices = std::vector<Vertex>();
			_Indices = std::vector<DWORD>();
		}

		ModelRaw(const ModelRaw& r) {
			_Vertices = std::vector<Vertex>();
			_Indices = std::vector<DWORD>();

			this->_Vertices.insert(this->_Vertices.begin(),
				r._Vertices.begin(), r._Vertices.end());
			this->_Indices.insert(this->_Indices.begin(),
				r._Indices.begin(), r._Indices.end());
		}

		ModelRaw(Vertex* v, DWORD* i, UINT vc, UINT ic) {
			_Vertices = std::vector<Vertex>();
			_Indices = std::vector<DWORD>();

			this->_Vertices.assign(v, v + vc);
			this->_Indices.assign(i, i + ic);
		}
	};

	class D3D11F_API Model {
	public:
		Model() {
			_Raw = ModelRaw();

			SetRotation(0, 0, 0);
			SetPosition(0, 0, 0);
		};

		bool Initialize(ID3D11Device* _Device, ID3D11DeviceContext* _DeviceContext, ID3D11ShaderResourceView* _Texture, ConstantBuffer<CB_VS_VertexShader>* _Vertexshader);
		void SetTexture(ID3D11ShaderResourceView* _Texture);
		virtual void Draw(const XMMATRIX& viewProjectionMatrix);
		void SetRawData(ModelRaw& r) {
			this->_Raw._Vertices.clear();
			this->_Raw._Indices.clear();

			this->_Raw._Vertices.insert(this->_Raw._Vertices.begin(),
				r._Vertices.begin(), r._Vertices.end());
			this->_Raw._Indices.insert(this->_Raw._Indices.begin(),
				r._Indices.begin(), r._Indices.end());
		}


		const XMVECTOR& GetPositionVector() const;
		const XMFLOAT3& GetPositionFloat3() const;
		const XMVECTOR& GetRotationVector() const;
		const XMFLOAT3& GetRotationFloat3() const;

		void SetPosition(const XMVECTOR& pos);
		void SetPosition(const XMFLOAT3& pos);
		void SetPosition(float x, float y, float z);
		void AdjustPosition(const XMVECTOR& pos);
		void AdjustPosition(const XMFLOAT3& pos);
		void AdjustPosition(float x, float y, float z);
		void SetRotation(const XMVECTOR& rot);
		void SetRotation(const XMFLOAT3& rot);
		void SetRotation(float x, float y, float z);
		void AdjustRotation(const XMVECTOR& rot);
		void AdjustRotation(const XMFLOAT3& rot);
		void AdjustRotation(float x, float y, float z);
		void SetLookAtPos(XMFLOAT3 lookAtPos);
		const XMVECTOR& GetForwardVector();
		const XMVECTOR& GetRightVector();
		const XMVECTOR& GetBackwardVector();
		const XMVECTOR& GetLeftVector();

		ConstantBuffer<CB_VS_VertexShader> * GetConstantBuffer() const {
			return this->_VertexShader;
		}

		void PrintCurrentRot();
		void PrintCurrentPos();
	protected:
		void UpdateWorldMatrix();

		ID3D11Device* _Device = nullptr;
		ID3D11DeviceContext* _DeviceContext = nullptr;
		ConstantBuffer<CB_VS_VertexShader>* _VertexShader = nullptr;
		ID3D11ShaderResourceView* _Texture = nullptr;

		ModelRaw _Raw;

		VertexBuffer<Vertex> _VertexBuffer;
		IndexBuffer _IndexBuffer;

		XMMATRIX _WorldMatrix = XMMatrixIdentity();

		XMVECTOR _PosVector;
		XMVECTOR _RotVector;
		XMFLOAT3 _Pos;
		XMFLOAT3 _Rot;

		const XMVECTOR DEFAULT_FORWARD_VECTOR = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);
		const XMVECTOR DEFAULT_UP_VECTOR = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
		const XMVECTOR DEFAULT_BACKWARD_VECTOR = XMVectorSet(0.0f, 0.0f, -1.0f, 0.0f);
		const XMVECTOR DEFAULT_LEFT_VECTOR = XMVectorSet(-1.0f, 0.0f, 0.0f, 0.0f);
		const XMVECTOR DEFAULT_RIGHT_VECTOR = XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f);

		XMVECTOR _VecForward;
		XMVECTOR _VecLeft;
		XMVECTOR _VecRight;
		XMVECTOR _VecBackward;
	};
}