#pragma once

#include "..\\pch.h"

namespace D3D11Framework {

	class D3D11F_API IndexBuffer {
	private:
		IndexBuffer(const IndexBuffer& rhs);

	private:
		ID3D11Buffer* _Buffer;
		UINT _BufferSize = 0;
	public:
		IndexBuffer() {}

		ID3D11Buffer* Get()const {
			return _Buffer;
		}

		ID3D11Buffer* const* GetAddressOf()const {
			return &_Buffer;
		}

		UINT BufferSize() const {
			return this->_BufferSize;
		}

		HRESULT Initialize(ID3D11Device* _Device, DWORD* data, UINT numIndices) {
			this->_BufferSize = numIndices;
			//Load Index Data
			D3D11_BUFFER_DESC indexBufferDesc;
			ZeroMemory(&indexBufferDesc, sizeof(indexBufferDesc));
			indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
			indexBufferDesc.ByteWidth = sizeof(DWORD) * numIndices;
			indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
			indexBufferDesc.CPUAccessFlags = 0;
			indexBufferDesc.MiscFlags = 0;

			D3D11_SUBRESOURCE_DATA indexBufferData;
			indexBufferData.pSysMem = data;
			HRESULT hr = _Device->CreateBuffer(&indexBufferDesc, &indexBufferData, &_Buffer);
			return hr;
		}
	};

}