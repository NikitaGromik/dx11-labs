#pragma once
#include "..\\pch.h"
#include <DirectXMath.h>

namespace D3D11Framework {
	struct D3D11F_API Vertex {
		Vertex() : _Position(0, 0, 0), _Color(0, 0, 0) {}
		Vertex(float x, float y, float z, float r, float g, float b)
			: _Position(x, y, z), _Color(r, g, b) {}

		DirectX::XMFLOAT3 _Position;
		DirectX::XMFLOAT3 _Color;
	};
}
