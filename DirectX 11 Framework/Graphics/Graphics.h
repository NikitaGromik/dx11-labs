#pragma once

#include "..\\pch.h"
#include "AdapterReader.h"
#include "Shaders.h"
#include "Vertex.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "ConstantBuffer.h"
#include "ConstantBufferType.h"
#include "Camera.h"
#include "Model.h"

namespace D3D11Framework {

	class D3D11F_API Graphics {
	public:
		Graphics();

		bool Initialize(HWND hwnd, int width, int height);

		bool InitializeModels();

		void RenderFrame();

		void AddModel(Model* m);

		Camera _Camera;

		std::vector<Model*> _Models;
	private:
		bool InitializeDirectX(HWND hwnd);
		bool InitializeShaders();
		bool InitializeScene();

		// Create buffers.
		ID3D11Device* _Device;

		// Set different resources for rendering.
		ID3D11DeviceContext* _DeviceContext;

		IDXGISwapChain* _SwapChain;

		// Second buffer (background).
		ID3D11RenderTargetView* _RenderTargetView;

		VertexShader _VertexShader;

		PixelShader _PixelShader;

		ConstantBuffer<CB_VS_VertexShader> _ConstantBuffer;

		ID3D11DepthStencilView* _DepthStencilView;
		ID3D11Texture2D* _DepthStencilBuffer;
		ID3D11DepthStencilState* _DepthStencilState;

		ID3D11RasterizerState* _RasterizerState;

		int _WindowWidth;
		int _WindowHeight;
	};
}
