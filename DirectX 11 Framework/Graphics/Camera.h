#pragma once

#include "..\\pch.h"
#include <DirectXMath.h>
using namespace DirectX;

namespace D3D11Framework {

	class D3D11F_API Camera {
	public:
		Camera();
		void SetProjectionValues(float fovDegrees, float aspectRatio, float nearZ, float farZ);

		const XMMATRIX& GetViewMatrix() const;
		const XMMATRIX& GetProjectionMatrix() const;

		const XMVECTOR& GetPositionVector() const;
		const XMFLOAT3& GetPositionFloat3() const;
		const XMVECTOR& GetRotationVector() const;
		const XMFLOAT3& GetRotationFloat3() const;

		const XMVECTOR& GetForwardVector();
		const XMVECTOR& GetRightVector();
		const XMVECTOR& GetBackwardVector();
		const XMVECTOR& GetLeftVector();

		void SetPosition(const XMVECTOR& _Pos);
		void SetPosition(float x, float y, float z);
		void AdjustPosition(const XMVECTOR& _Pos);
		void AdjustPosition(float x, float y, float z);
		void SetRotation(const XMVECTOR& _Rot);
		void SetRotation(float x, float y, float z);
		void AdjustRotation(const XMVECTOR& _Rot);
		void AdjustRotation(float x, float y, float z);
		void SetLookAtPos(XMFLOAT3 lookAtPos);
	private:
		void UpdateViewMatrix();
		XMVECTOR _PosVector;
		XMVECTOR _RotVector;
		XMFLOAT3 _Pos;
		XMFLOAT3 _Rot;
		XMMATRIX _ViewMatrix;
		XMMATRIX _ProjectionMatrix;

		const XMVECTOR DEFAULT_FORWARD_VECTOR = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);
		const XMVECTOR DEFAULT_UP_VECTOR = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
		const XMVECTOR DEFAULT_BACKWARD_VECTOR = XMVectorSet(0.0f, 0.0f, -1.0f, 0.0f);
		const XMVECTOR DEFAULT_LEFT_VECTOR = XMVectorSet(-1.0f, 0.0f, 0.0f, 0.0f);
		const XMVECTOR DEFAULT_RIGHT_VECTOR = XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f);

		XMVECTOR _VecForward;
		XMVECTOR _VecLeft;
		XMVECTOR _VecRight;
		XMVECTOR _VecBackward;
	};
}
