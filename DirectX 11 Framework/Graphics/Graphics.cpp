#include "..\\pch.h"
#include "Graphics.h"
#include "../Log.h"

D3D11Framework::Graphics::Graphics() :
	_Device(nullptr),
	_DeviceContext(nullptr),
	_SwapChain(nullptr),
	_RenderTargetView(nullptr) {
	// ..
}

bool D3D11Framework::Graphics::Initialize(HWND hwnd, int width, int height) {
	this->_WindowWidth = width;
	this->_WindowHeight = height;

	if (!InitializeDirectX(hwnd)) {
		return false;
	}

	if (!InitializeShaders()) {
		return false;
	}

	if (!InitializeScene()) {
		return false;
	}

	return true;
}

bool D3D11Framework::Graphics::InitializeModels() {
	for (auto it = _Models.begin(); it != _Models.end(); it++) {
		if (!(*it)->Initialize(this->_Device, this->_DeviceContext, NULL, &this->_ConstantBuffer))
			return false;
	}
	return true;
}

void D3D11Framework::Graphics::RenderFrame() {
	float bgcolor[] = {0.0f, 0.0f, 0.0f, 1.0f};
	this->_DeviceContext->ClearRenderTargetView(this->_RenderTargetView, bgcolor);
	this->_DeviceContext->ClearDepthStencilView(
		this->_DepthStencilView,
		D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
		1.0f, 0
	);

	this->_DeviceContext->IASetInputLayout(this->_VertexShader.GetInputLayout());
	this->_DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY::D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	this->_DeviceContext->RSSetState(this->_RasterizerState);
	this->_DeviceContext->OMSetDepthStencilState(this->_DepthStencilState, 0);
	this->_DeviceContext->VSSetShader(_VertexShader.GetShader(), NULL, 0);
	this->_DeviceContext->PSSetShader(_PixelShader.GetShader(), NULL, 0);

	{
		for (auto it = _Models.begin(); it != _Models.end(); it++) {
			(*it)->Draw(_Camera.GetViewMatrix() * _Camera.GetProjectionMatrix());
		}
	}

	this->_SwapChain->Present(1, NULL);
}

void D3D11Framework::Graphics::AddModel(Model* m) {
	_Models.push_back(m);
}

bool D3D11Framework::Graphics::InitializeDirectX(HWND hwnd) {
	std::vector<AdapterData> adapters = AdapterReader::GetAdapters();

	if (adapters.size() < 1) {
		AlertUtils::E("Video (IDXGI) Adapters weren't found.");
		return false;
	}

	DXGI_SWAP_CHAIN_DESC scd;
	ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC));

	scd.BufferDesc.Width = this->_WindowWidth;
	scd.BufferDesc.Height = this->_WindowHeight;
	scd.BufferDesc.RefreshRate.Numerator = 60;
	scd.BufferDesc.RefreshRate.Denominator = 1;
	scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	scd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	scd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	scd.SampleDesc.Count = 1;
	scd.SampleDesc.Quality = 0;

	scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	scd.BufferCount = 1;
	scd.OutputWindow = hwnd;
	scd.Windowed = TRUE;
	scd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	scd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	HRESULT hr;
	hr = D3D11CreateDeviceAndSwapChain(
		adapters[0].pAdapter, // IDXGI Adapter (VideoCard).
		D3D_DRIVER_TYPE_UNKNOWN,
		NULL, // Software driver type.
		NULL, // Runtime layers.
		NULL, // Feature levels array.
		0, // Size of features array.
		D3D11_SDK_VERSION,
		&scd, // Swap chain description.
		&this->_SwapChain,
		&this->_Device,
		NULL, // Supported feature level.
		&this->_DeviceContext
	);

	if (FAILED(hr)) {
		AlertUtils::E(hr, "Failed to create device and swapchain.");
		return false;
	}

	ID3D11Texture2D* backBuffer = nullptr;
	hr = this->_SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backBuffer));
	if (FAILED(hr)) {
		AlertUtils::E(hr, "GetBuffer Failed.");
		return false;
	}

	hr = this->_Device->CreateRenderTargetView(backBuffer, NULL, &this->_RenderTargetView);
	if (FAILED(hr)) {
		AlertUtils::E(hr, "Failed to create render target view.");
		return false;
	}

	// Describe Depth/Stencil Buffer.
	D3D11_TEXTURE2D_DESC depthStencilDesc;
	depthStencilDesc.Width = this->_WindowWidth;
	depthStencilDesc.Height = this->_WindowHeight;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilDesc.SampleDesc.Count = 1;
	depthStencilDesc.SampleDesc.Quality = 0;
	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags = 0;

	hr = this->_Device->CreateTexture2D(&depthStencilDesc, NULL, &this->_DepthStencilBuffer);
	if (FAILED(hr)) {
		AlertUtils::E(hr, "Failed to create depth stencil buffer.");
		return false;
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV = {};
	descDSV.Format = depthStencilDesc.Format;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;
	hr = this->_Device->CreateDepthStencilView(this->_DepthStencilBuffer, &descDSV, &this->_DepthStencilView);
	if (FAILED(hr)) {
		AlertUtils::E(hr, "Failed to create depth stencil view.");
		return false;
	}

	// Output Merger.
	this->_DeviceContext->OMSetRenderTargets(1, &this->_RenderTargetView, this->_DepthStencilView);

	// Create depth stencil state.
	D3D11_DEPTH_STENCIL_DESC depthstencildesc;
	ZeroMemory(&depthstencildesc, sizeof(D3D11_DEPTH_STENCIL_DESC));

	depthstencildesc.DepthEnable = true;
	depthstencildesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK::D3D11_DEPTH_WRITE_MASK_ALL;
	depthstencildesc.DepthFunc = D3D11_COMPARISON_FUNC::D3D11_COMPARISON_LESS;

	hr = this->_Device->CreateDepthStencilState(&depthstencildesc, &this->_DepthStencilState);
	if (FAILED(hr)) {
		AlertUtils::E(hr, "Failed to create depth stencil state.");
		return false;
	}

	// Create the Viewport.
	D3D11_VIEWPORT viewport;
	ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));

	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	viewport.Width = this->_WindowWidth;
	viewport.Height = this->_WindowHeight;
	viewport.MinDepth = 0.f; // Close.
	viewport.MaxDepth = 1.f; // Far.

	// Set the Viewport.
	this->_DeviceContext->RSSetViewports(1, &viewport);

	// Create Rasterizer State.
	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));

	rasterizerDesc.FillMode = D3D11_FILL_MODE::D3D11_FILL_SOLID;
	rasterizerDesc.CullMode = D3D11_CULL_MODE::D3D11_CULL_BACK;
	//rasterizerDesc.FrontCounterClockwise = TRUE;
	hr = this->_Device->CreateRasterizerState(&rasterizerDesc, &this->_RasterizerState);
	if (FAILED(hr)) {
		AlertUtils::E(hr, "Failed to create rasterizer state.");
		return false;
	}

	return true;

}

bool D3D11Framework::Graphics::InitializeShaders() {
	// Input Assembler.
	D3D11_INPUT_ELEMENT_DESC layout[] = {
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{"COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	UINT count = ARRAYSIZE(layout);

	// Vertex shader.
	if (!this->_VertexShader.Initialize(this->_Device, L".\\VertexShader.cso", layout, count)) {
		return false;
	}

	// Pixel shader.
	if (!this->_PixelShader.Initialize(this->_Device, L".\\PixelShader.cso")) {
		return false;
	}


	return true;
}

bool D3D11Framework::Graphics::InitializeScene() {
	// Initialize Constant Buffer(s).
	HRESULT hr = this->_ConstantBuffer.Initialize(this->_Device, this->_DeviceContext);
	if (FAILED(hr)) {
		AlertUtils::E(hr, "Failed to initialize constant buffer.");
		return false;
	}

	_Camera.SetPosition(0.f, 0.f, -2.f);
	_Camera.SetProjectionValues(120.f, static_cast<float>(this->_WindowWidth) / static_cast<float>(this->_WindowHeight), 0.01f, 1000.f);

	return true;
}
