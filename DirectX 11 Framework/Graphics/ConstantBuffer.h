#pragma once

#include "..\\pch.h"
#include "..\\AlertUtils.h"

namespace D3D11Framework {

	template<class T>
	class D3D11F_API ConstantBuffer {
	private:
		ConstantBuffer(const ConstantBuffer<T>& rhs);

	private:

		ID3D11Buffer* _Buffer = nullptr;
		ID3D11DeviceContext* _DeviceContext = nullptr;

	public:
		ConstantBuffer() {}

		T _Data;

		ID3D11Buffer* Get()const {
			return _Buffer;
		}

		ID3D11Buffer* Get() {
			return _Buffer;
		}

		ID3D11Buffer* const* GetAddressOf()const {
			return &_Buffer;
		}

		HRESULT Initialize(ID3D11Device* _Device, ID3D11DeviceContext* _DeviceContext) {
			this->_DeviceContext = _DeviceContext;

			D3D11_BUFFER_DESC desc;
			desc.Usage = D3D11_USAGE_DYNAMIC;
			desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			desc.MiscFlags = 0;
			desc.ByteWidth = static_cast<UINT>(sizeof(T) + (16 - (sizeof(T) % 16)));
			desc.StructureByteStride = 0;

			HRESULT hr = _Device->CreateBuffer(&desc, 0, &_Buffer);
			return hr;
		}

		bool ApplyChanges() {
			D3D11_MAPPED_SUBRESOURCE mappedResource;
			HRESULT hr = this->_DeviceContext->Map(_Buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
			if (FAILED(hr)) {
				AlertUtils::E(hr, "Failed to map constant _Buffer.");
				return false;
			}
			CopyMemory(mappedResource.pData, &_Data, sizeof(T));
			this->_DeviceContext->Unmap(_Buffer, 0);
			return true;
		}
	};


}