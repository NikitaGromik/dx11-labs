#include "..\\pch.h"
#include "Model.h"
#include "..\Log.h"

namespace D3D11Framework {

	bool Model::Initialize(ID3D11Device* _Device, ID3D11DeviceContext* _DeviceContext, ID3D11ShaderResourceView* _Texture, ConstantBuffer<CB_VS_VertexShader>* _VertexShader) {
		this->_Device = _Device;
		this->_DeviceContext = _DeviceContext;
		this->_Texture = _Texture;
		this->_VertexShader = _VertexShader;

		//Load Vertex Data
		HRESULT hr = this->_VertexBuffer.Initialize(this->_Device, &_Raw._Vertices[0], _Raw._Vertices.size());
		if (FAILED(hr)) {
			AlertUtils::E(hr, "Failed to create vertex buffer");
			return false;
		}

		//Load Index Data
		hr = this->_IndexBuffer.Initialize(this->_Device, &_Raw._Indices[0], _Raw._Indices.size());
		if (FAILED(hr)) {
			AlertUtils::E(hr, "Failed to create index buffer");
			return false;
		}

		this->UpdateWorldMatrix();
		return true;
	}

	void Model::SetTexture(ID3D11ShaderResourceView* _Texture) {
		this->_Texture = _Texture;
	}

	void Model::Draw(const XMMATRIX& viewProjectionMatrix) {
		//Update Constant buffer with WVP Matrix
		this->_VertexShader->_Data._Matrix = this->_WorldMatrix * viewProjectionMatrix; //Calculate World-View-Projection Matrix
		this->_VertexShader->_Data._Matrix = XMMatrixTranspose(this->_VertexShader->_Data._Matrix);
		this->_VertexShader->ApplyChanges();
		this->_DeviceContext->VSSetConstantBuffers(0, 1, this->_VertexShader->GetAddressOf());

		//this->deviceContext->PSSetShaderResources(0, 1, &this->texture); //Set Texture
		this->_DeviceContext->IASetIndexBuffer(this->_IndexBuffer.Get(), DXGI_FORMAT::DXGI_FORMAT_R32_UINT, 0);
		UINT offset = 0;
		this->_DeviceContext->IASetVertexBuffers(0, 1, this->_VertexBuffer.GetAddressOf(), this->_VertexBuffer.StridePtr(), &offset);
		this->_DeviceContext->DrawIndexed(this->_IndexBuffer.BufferSize(), 0, 0); //Draw

		this->PrintCurrentRot();
	}

	void Model::UpdateWorldMatrix() {
		this->_WorldMatrix = XMMatrixRotationRollPitchYaw(this->_Rot.x, this->_Rot.y, this->_Rot.z) * XMMatrixTranslation(this->_Pos.x, this->_Pos.y, this->_Pos.z);
		XMMATRIX vecRotationMatrix = XMMatrixRotationRollPitchYaw(0.0f, this->_Rot.y, 0.0f);
		this->_VecForward = XMVector3TransformCoord(this->DEFAULT_FORWARD_VECTOR, vecRotationMatrix);
		this->_VecBackward = XMVector3TransformCoord(this->DEFAULT_BACKWARD_VECTOR, vecRotationMatrix);
		this->_VecLeft = XMVector3TransformCoord(this->DEFAULT_LEFT_VECTOR, vecRotationMatrix);
		this->_VecRight = XMVector3TransformCoord(this->DEFAULT_RIGHT_VECTOR, vecRotationMatrix);
	}

	void Model::PrintCurrentRot() {
		std::string w = "Current rotation: X: ";
		w += std::to_string(_Rot.x);
		w += " Y: ";
		w += std::to_string(_Rot.y);
		w += " Z: ";
		w += std::to_string(_Rot.z);
		w += "\n";

		OutputDebugStringA(w.c_str());
	}

	void Model::PrintCurrentPos() {
		std::string w = "Current position: X: ";
		w += std::to_string(_Pos.x);
		w += " Y: ";
		w += std::to_string(_Pos.y);
		w += " Z: ";
		w += std::to_string(_Pos.z);
		w += "\n";

		OutputDebugStringA(w.c_str());
	}

	const XMVECTOR& Model::GetPositionVector() const {
		return this->_PosVector;
	}

	const XMFLOAT3& Model::GetPositionFloat3() const {
		return this->_Pos;
	}

	const XMVECTOR& Model::GetRotationVector() const {
		return this->_RotVector;
	}

	const XMFLOAT3& Model::GetRotationFloat3() const {
		return this->_Rot;
	}

	void Model::SetPosition(const XMVECTOR& _Pos) {
		XMStoreFloat3(&this->_Pos, _Pos);
		this->_PosVector = _Pos;
		this->UpdateWorldMatrix();
	}

	void Model::SetPosition(const XMFLOAT3& _Pos) {
		this->_Pos = _Pos;
		this->_PosVector = XMLoadFloat3(&this->_Pos);
		this->UpdateWorldMatrix();
	}

	void Model::SetPosition(float x, float y, float z) {
		this->_Pos = XMFLOAT3(x, y, z);
		this->_PosVector = XMLoadFloat3(&this->_Pos);
		this->UpdateWorldMatrix();
	}

	void Model::AdjustPosition(const XMVECTOR& _Pos) {
		this->_PosVector += _Pos;
		XMStoreFloat3(&this->_Pos, this->_PosVector);
		this->UpdateWorldMatrix();
	}

	void Model::AdjustPosition(const XMFLOAT3& _Pos) {
		this->_Pos.x += _Pos.y;
		this->_Pos.y += _Pos.y;
		this->_Pos.z += _Pos.z;
		this->_PosVector = XMLoadFloat3(&this->_Pos);
		this->UpdateWorldMatrix();
	}

	void Model::AdjustPosition(float x, float y, float z) {
		this->_Pos.x += x;
		this->_Pos.y += y;
		this->_Pos.z += z;
		this->_PosVector = XMLoadFloat3(&this->_Pos);
		this->UpdateWorldMatrix();
	}

	void Model::SetRotation(const XMVECTOR& _Rot) {
		this->_RotVector = _Rot;
		XMStoreFloat3(&this->_Rot, _Rot);
		this->UpdateWorldMatrix();
	}

	void Model::SetRotation(const XMFLOAT3& _Rot) {
		this->_Rot = _Rot;
		this->_RotVector = XMLoadFloat3(&this->_Rot);
		this->UpdateWorldMatrix();
	}

	void Model::SetRotation(float x, float y, float z) {
		this->_Rot = XMFLOAT3(x, y, z);
		this->_RotVector = XMLoadFloat3(&this->_Rot);
		this->UpdateWorldMatrix();
	}

	void Model::AdjustRotation(const XMVECTOR& _Rot) {
		this->_RotVector += _Rot;
		XMStoreFloat3(&this->_Rot, this->_RotVector);
		this->UpdateWorldMatrix();
	}

	void Model::AdjustRotation(const XMFLOAT3& _Rot) {
		this->_Rot.x += _Rot.x;
		this->_Rot.y += _Rot.y;
		this->_Rot.z += _Rot.z;
		this->_RotVector = XMLoadFloat3(&this->_Rot);
		this->UpdateWorldMatrix();
	}

	void Model::AdjustRotation(float x, float y, float z) {
		this->_Rot.x += x;
		this->_Rot.y += y;
		this->_Rot.z += z;
		this->_RotVector = XMLoadFloat3(&this->_Rot);

		this->UpdateWorldMatrix();
	}

	void Model::SetLookAtPos(XMFLOAT3 lookAtPos) {
		//Verify that look at pos is not the same as cam pos. They cannot be the same as that wouldn't make sense and would result in undefined behavior.
		if (lookAtPos.x == this->_Pos.x && lookAtPos.y == this->_Pos.y && lookAtPos.z == this->_Pos.z)
			return;

		lookAtPos.x = this->_Pos.x - lookAtPos.x;
		lookAtPos.y = this->_Pos.y - lookAtPos.y;
		lookAtPos.z = this->_Pos.z - lookAtPos.z;

		float pitch = 0.0f;
		if (lookAtPos.y != 0.0f) {
			const float distance = sqrt(lookAtPos.x * lookAtPos.x + lookAtPos.z * lookAtPos.z);
			pitch = atan(lookAtPos.y / distance);
		}

		float yaw = 0.0f;
		if (lookAtPos.x != 0.0f) {
			yaw = atan(lookAtPos.x / lookAtPos.z);
		}
		if (lookAtPos.z > 0)
			yaw += XM_PI;

		this->SetRotation(pitch, yaw, 0.0f);
	}

	const XMVECTOR& Model::GetForwardVector() {
		return this->_VecForward;
	}

	const XMVECTOR& Model::GetRightVector() {
		return this->_VecRight;
	}

	const XMVECTOR& Model::GetBackwardVector() {
		return this->_VecBackward;
	}

	const XMVECTOR& Model::GetLeftVector() {
		return this->_VecLeft;
	}
}