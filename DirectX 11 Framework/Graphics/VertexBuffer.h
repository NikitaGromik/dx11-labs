#pragma once

#include "..\\pch.h"

namespace D3D11Framework {

	template<class T>
	class D3D11F_API VertexBuffer {
	private:
		VertexBuffer(const VertexBuffer<T>& rhs);

		ID3D11Buffer* _Buffer;
		UINT* _Stride;
		UINT _BufferSize = 0;

	public:
		VertexBuffer() {
			_Buffer = nullptr;
			_Stride = nullptr;
		}

		ID3D11Buffer* Get()const {
			return _Buffer;
		}

		ID3D11Buffer* const* GetAddressOf()const {
			return &_Buffer;
		}

		ID3D11Buffer** GetAddressOf() {
			return &_Buffer;
		}

		UINT BufferSize() const {
			return this->_BufferSize;
		}

		const UINT Stride() const {
			return *this->_Stride.get();
		}

		const UINT* StridePtr() const {
			return this->_Stride;
		}

		HRESULT Initialize(ID3D11Device* _Device, T* data, UINT numVertices) {
			this->_BufferSize = numVertices;
			this->_Stride = new UINT(sizeof(T));

			D3D11_BUFFER_DESC vertexBufferDesc;
			ZeroMemory(&vertexBufferDesc, sizeof(vertexBufferDesc));

			vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
			vertexBufferDesc.ByteWidth = sizeof(T) * numVertices;
			vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			vertexBufferDesc.CPUAccessFlags = 0;
			vertexBufferDesc.MiscFlags = 0;

			D3D11_SUBRESOURCE_DATA vertexBufferData;
			ZeroMemory(&vertexBufferData, sizeof(vertexBufferData));
			vertexBufferData.pSysMem = data;

			HRESULT hr = _Device->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &this->_Buffer);
			return hr;
		}
	};

}
