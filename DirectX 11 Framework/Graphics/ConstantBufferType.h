#pragma once

#include "..\\pch.h"
#include <DirectXMath.h>

namespace D3D11Framework {

	struct CB_VS_VertexShader {
		DirectX::XMMATRIX _Matrix;
	};
}
