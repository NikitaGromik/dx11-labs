#include "..\\pch.h"
#include "Camera.h"

namespace D3D11Framework {

	Camera::Camera() {
		this->_Pos = XMFLOAT3(0.0f, 0.0f, 0.0f);
		this->_PosVector = XMLoadFloat3(&this->_Pos);
		this->_Rot = XMFLOAT3(0.0f, 0.0f, 0.0f);
		this->_RotVector = XMLoadFloat3(&this->_Rot);
		this->UpdateViewMatrix();
	}

	void Camera::SetProjectionValues(float fovDegrees, float aspectRatio, float nearZ, float farZ) {
		float fovRadians = (fovDegrees / 360.0f) * XM_2PI;
		this->_ProjectionMatrix = XMMatrixPerspectiveFovLH(fovRadians, aspectRatio, nearZ, farZ);
	}

	const XMMATRIX& Camera::GetViewMatrix() const {
		return this->_ViewMatrix;
	}

	const XMMATRIX& Camera::GetProjectionMatrix() const {
		return this->_ProjectionMatrix;
	}

	const XMVECTOR& Camera::GetPositionVector() const {
		return this->_PosVector;
	}

	const XMFLOAT3& Camera::GetPositionFloat3() const {
		return this->_Pos;
	}

	const XMVECTOR& Camera::GetRotationVector() const {
		return this->_RotVector;
	}

	const XMFLOAT3& Camera::GetRotationFloat3() const {
		return this->_Rot;
	}

	void Camera::SetPosition(const XMVECTOR& _Pos) {
		XMStoreFloat3(&this->_Pos, _Pos);
		this->_PosVector = _Pos;
		this->UpdateViewMatrix();
	}

	void Camera::SetPosition(float x, float y, float z) {
		this->_Pos = XMFLOAT3(x, y, z);
		this->_PosVector = XMLoadFloat3(&this->_Pos);
		this->UpdateViewMatrix();
	}

	void Camera::AdjustPosition(const XMVECTOR& _Pos) {
		this->_PosVector += _Pos;
		XMStoreFloat3(&this->_Pos, this->_PosVector);
		this->UpdateViewMatrix();
	}

	void Camera::AdjustPosition(float x, float y, float z) {
		this->_Pos.x += x;
		this->_Pos.y += y;
		this->_Pos.z += z;
		this->_PosVector = XMLoadFloat3(&this->_Pos);
		this->UpdateViewMatrix();
	}

	void Camera::SetRotation(const XMVECTOR& _Rot) {
		this->_RotVector = _Rot;
		XMStoreFloat3(&this->_Rot, _Rot);
		this->UpdateViewMatrix();
	}

	void Camera::SetRotation(float x, float y, float z) {
		this->_Rot = XMFLOAT3(x, y, z);
		this->_RotVector = XMLoadFloat3(&this->_Rot);
		this->UpdateViewMatrix();
	}

	void Camera::AdjustRotation(const XMVECTOR& _Rot) {
		this->_RotVector += _Rot;
		XMStoreFloat3(&this->_Rot, this->_RotVector);
		this->UpdateViewMatrix();
	}

	void Camera::AdjustRotation(float x, float y, float z) {
		this->_Rot.x += x;
		this->_Rot.y += y;
		this->_Rot.z += z;
		this->_RotVector = XMLoadFloat3(&this->_Rot);
		this->UpdateViewMatrix();
	}

	void Camera::SetLookAtPos(XMFLOAT3 lookAtPos) {
		// Verify that look at pos is not the same as cam pos. 
		// They cannot be the same as that wouldn't make sense and would result in undefined behavior.
		if (lookAtPos.x == this->_Pos.x && lookAtPos.y == this->_Pos.y && lookAtPos.z == this->_Pos.z)
			return;

		lookAtPos.x = this->_Pos.x - lookAtPos.x;
		lookAtPos.y = this->_Pos.y - lookAtPos.y;
		lookAtPos.z = this->_Pos.z - lookAtPos.z;

		float pitch = 0.0f;
		if (lookAtPos.y != 0.0f) 	{
			const float distance = sqrt(lookAtPos.x * lookAtPos.x + lookAtPos.z * lookAtPos.z);
			pitch = atan(lookAtPos.y / distance);
		}

		float yaw = 0.0f;
		if (lookAtPos.x != 0.0f) 	{
			yaw = atan(lookAtPos.x / lookAtPos.z);
		}
		if (lookAtPos.z > 0)
			yaw += XM_PI;

		this->SetRotation(pitch, yaw, 0.0f);
	}

	const XMVECTOR& Camera::GetForwardVector() {
		return this->_VecForward;
	}

	const XMVECTOR& Camera::GetRightVector() {
		return this->_VecRight;
	}

	const XMVECTOR& Camera::GetBackwardVector() {
		return this->_VecBackward;
	}

	const XMVECTOR& Camera::GetLeftVector() {
		return this->_VecLeft;
	}

	void Camera::UpdateViewMatrix() //Updates view matrix and also updates the movement vectors
	{
		//Calculate camera rotation matrix
		XMMATRIX camRotationMatrix = XMMatrixRotationRollPitchYaw(this->_Rot.x, this->_Rot.y, this->_Rot.z);
		//Calculate unit vector of cam target based off camera forward value transformed by cam rotation matrix
		XMVECTOR camTarget = XMVector3TransformCoord(this->DEFAULT_FORWARD_VECTOR, camRotationMatrix);
		//Adjust cam target to be offset by the camera's current position
		camTarget += this->_PosVector;
		//Calculate up direction based on current rotation
		XMVECTOR upDir = XMVector3TransformCoord(this->DEFAULT_UP_VECTOR, camRotationMatrix);
		//Rebuild view matrix
		this->_ViewMatrix = XMMatrixLookAtLH(this->_PosVector, camTarget, upDir);

		XMMATRIX vecRotationMatrix = XMMatrixRotationRollPitchYaw(0.0f, this->_Rot.y, 0.0f);
		this->_VecForward = XMVector3TransformCoord(this->DEFAULT_FORWARD_VECTOR, vecRotationMatrix);
		this->_VecBackward = XMVector3TransformCoord(this->DEFAULT_BACKWARD_VECTOR, vecRotationMatrix);
		this->_VecLeft = XMVector3TransformCoord(this->DEFAULT_LEFT_VECTOR, vecRotationMatrix);
		this->_VecRight = XMVector3TransformCoord(this->DEFAULT_RIGHT_VECTOR, vecRotationMatrix);
	}
}
