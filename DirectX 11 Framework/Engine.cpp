#include "pch.h"
#include "Engine.h"
#include "Log.h"

namespace D3D11Framework {
	bool Engine::Initialize(HINSTANCE hInstance, std::string windowTitle, std::string windowClass, int width, int height) {
		if (!this->_RenderWindow.Initialize(this, hInstance, windowTitle, windowClass, width, height))
			return false;

		if (!this->_GFX.Initialize(this->_RenderWindow.GetHWND(), width, height))
			return false;

		return true;
	}

	bool Engine::ProcessMessages() {
		return this->_RenderWindow.ProcessMessages();
	}

	void Engine::Update() {
		while (!_Keyboard.CharBufferIsEmpty()) 	{
			unsigned char ch = _Keyboard.ReadChar();
		}

		while (!_Keyboard.KeyBufferIsEmpty()) 	{
			KeyboardEvent kbe = _Keyboard.ReadKey();
			unsigned char keycode = kbe.GetKeyCode();
		}

		const float cameraSpeed = 0.02f;

		if (_Keyboard.KeyIsPressed('W')) {
			this->_GFX._Camera.AdjustPosition(this->_GFX._Camera.GetForwardVector() * cameraSpeed);
		}
		if (_Keyboard.KeyIsPressed('S')) {
			this->_GFX._Camera.AdjustPosition(this->_GFX._Camera.GetBackwardVector() * cameraSpeed);
		}
		if (_Keyboard.KeyIsPressed('A')) {
			this->_GFX._Camera.AdjustPosition(this->_GFX._Camera.GetLeftVector() * cameraSpeed);
		}
		if (_Keyboard.KeyIsPressed('D')) {
			this->_GFX._Camera.AdjustPosition(this->_GFX._Camera.GetRightVector() * cameraSpeed);
		}
		if (_Keyboard.KeyIsPressed(VK_SPACE)) {
			this->_GFX._Camera.AdjustPosition(0.0f, cameraSpeed, 0.0f);
		}
		if (_Keyboard.KeyIsPressed('Z')) {
			this->_GFX._Camera.AdjustPosition(0.0f, -cameraSpeed, 0.0f);
		}

		while (!_Mouse.EventBufferIsEmpty()) {
			MouseEvent event = _Mouse.ReadEvent();

			if (_Mouse.IsRightDown()) {
				if (event.GetType() == MouseEvent::EventType::RAW_MOVE) {
					this->_GFX._Camera.AdjustRotation(
						(float) event.GetPosY() * 0.01f,
						(float) event.GetPosX() * 0.01f,
						0
					);
				}
			}
		}
	}

	void Engine::Render() {
		this->_GFX.RenderFrame();
	}
}