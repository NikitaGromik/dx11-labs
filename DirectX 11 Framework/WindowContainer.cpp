#include "pch.h"
#include "WindowContainer.h"

namespace D3D11Framework {
	WindowContainer::WindowContainer() {
		static bool isRawInputInitialized = false;
		if (!isRawInputInitialized) {
			RAWINPUTDEVICE rid;

			rid.usUsagePage = 0x01; //Mouse
			rid.usUsage = 0x02;
			rid.dwFlags = 0;
			rid.hwndTarget = NULL;

			if (RegisterRawInputDevices(&rid, 1, sizeof(rid)) == FALSE) {
				AlertUtils::E(GetLastError(), "Failed to register raw input devices.");
				exit(-1);
			}

			isRawInputInitialized = true;
		}
	}

	LRESULT WindowContainer::WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
		switch (uMsg) {
			case WM_KEYDOWN:
			{
				unsigned char keycode = static_cast<unsigned char>(wParam);
				if (_Keyboard.IsKeysAutoRepeat()) {
					_Keyboard.OnKeyPressed(keycode);
				} else {
					// 31th bit.
					const bool wasPressed = lParam & 0x40000000;
					if (!wasPressed) {
						_Keyboard.OnKeyPressed(keycode);
					}
				}
				return 0;
			}
			case WM_KEYUP:
			{
				unsigned char keycode = static_cast<unsigned char>(wParam);
				_Keyboard.OnKeyReleased(keycode);
				return 0;
			}
			case WM_CHAR:
			{
				unsigned char ch = static_cast<unsigned char>(wParam);
				if (_Keyboard.IsCharsAutoRepeat()) {
					_Keyboard.OnChar(ch);
				} else {
					// 31th bit.
					const bool wasPressed = lParam & 0x40000000;
					if (!wasPressed) {
						_Keyboard.OnChar(ch);
					}
				}
				return 0;
			}

			// Mouse.
			case WM_MOUSEMOVE:
			{
				int x = LOWORD(lParam);
				int y = HIWORD(lParam);
				_Mouse.OnMouseMove(x, y);
				return 0;
			}
			case WM_LBUTTONDOWN:
			{
				int x = LOWORD(lParam);
				int y = HIWORD(lParam);
				_Mouse.OnLeftPressed(x, y);
				return 0;
			}
			case WM_RBUTTONDOWN:
			{
				int x = LOWORD(lParam);
				int y = HIWORD(lParam);
				_Mouse.OnRightPressed(x, y);
				return 0;
			}
			case WM_MBUTTONDOWN:
			{
				int x = LOWORD(lParam);
				int y = HIWORD(lParam);
				_Mouse.OnMiddlePressed(x, y);
				return 0;
			}
			case WM_LBUTTONUP:
			{
				int x = LOWORD(lParam);
				int y = HIWORD(lParam);
				_Mouse.OnLeftReleased(x, y);
				return 0;
			}
			case WM_RBUTTONUP:
			{
				int x = LOWORD(lParam);
				int y = HIWORD(lParam);
				_Mouse.OnRightReleased(x, y);
				return 0;
			}
			case WM_MBUTTONUP:
			{
				int x = LOWORD(lParam);
				int y = HIWORD(lParam);
				_Mouse.OnMiddleReleased(x, y);
				return 0;
			}
			case WM_MOUSEWHEEL:
			{
				int x = LOWORD(lParam);
				int y = HIWORD(lParam);
				if (GET_WHEEL_DELTA_WPARAM(wParam) > 0) {
					_Mouse.OnWheelUp(x, y);
				} else if (GET_WHEEL_DELTA_WPARAM(wParam) < 0) {
					_Mouse.OnWheelDown(x, y);
				}
				return 0;
			}
			case WM_INPUT:
			{
				UINT dataSize;
				GetRawInputData(reinterpret_cast<HRAWINPUT>(lParam), RID_INPUT, NULL, &dataSize, sizeof(RAWINPUTHEADER)); //Need to populate data size first

				if (dataSize > 0) {
					std::unique_ptr<BYTE[]> rawdata = std::make_unique<BYTE[]>(dataSize);
					if (GetRawInputData(reinterpret_cast<HRAWINPUT>(lParam), RID_INPUT, rawdata.get(), &dataSize, sizeof(RAWINPUTHEADER)) == dataSize) {
						RAWINPUT* raw = reinterpret_cast<RAWINPUT*>(rawdata.get());
						if (raw->header.dwType == RIM_TYPEMOUSE) {
							_Mouse.OnMouseMoveRaw(raw->data.mouse.lLastX, raw->data.mouse.lLastY);
						}
					}
				}

				return DefWindowProc(hwnd, uMsg, wParam, lParam); //Need to call DefWindowProc for WM_INPUT messages
			}
			default:
				return DefWindowProc(hwnd, uMsg, wParam, lParam);
		}
	}
}
