#include "pch.h"
#include "StringConverter.h"

namespace D3D11Framework {
	std::wstring StringConverter::StringToWide(std::string str) {
		return std::wstring(str.begin(), str.end());
	}
}
