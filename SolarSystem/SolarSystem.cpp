﻿// SolarSystem.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#pragma comment(linker, "/subsystem:windows /ENTRY:wWinMainCRTStartup")

#include <iostream>
#include <Log.h>
#include "SolarGame.h"


int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow) {
	D3D11Framework::Log::OnCreate();
	D3D11Framework::Log::P("Program started!");

	SolarGame game;
	if (game.Initialize(hInstance, "Solar System", "MyWindowClass", 800, 600)) {

		if (!game.InitializeModels()) {
			return -1;
		}

		while (game.ProcessMessages()) {
			game.Update();
			game.Render();
		}
	}

	D3D11Framework::Log::P("Program finished!");
	return 0;
}
