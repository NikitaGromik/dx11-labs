#pragma once

#include <Graphics/Model.h>

using namespace DirectX;

using namespace D3D11Framework;

class Sun : public Model {

public:
	Sun();
	void Draw(const XMMATRIX& viewProjectionMatrix) override;

};

