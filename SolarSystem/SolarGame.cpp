#include "SolarGame.h"

SolarGame::SolarGame() {
	_Sun = new Sun();
	D3D11Framework::ModelRaw sunRaw = D3D11Framework::ModelFactory::GetSphere(1, 40, 40);
	_Sun->SetRawData(sunRaw);

	_Mercury = new Planet(1.8f, 0.00250f);
	D3D11Framework::ModelRaw mercuryRaw = D3D11Framework::ModelFactory::GetBox(0.2f, 0.2f, 0.2f);
	_Mercury->SetRawData(mercuryRaw);

	_Venus = new Planet(2.5f, 0.00125f);
	D3D11Framework::ModelRaw venusRaw = D3D11Framework::ModelFactory::GetBox(0.4f, 0.4f, 0.4f);
	_Venus->SetRawData(venusRaw);

	_Earth = new Planet(3.6f, 0.0010f);
	D3D11Framework::ModelRaw earthRaw = D3D11Framework::ModelFactory::GetBox(0.6f, 0.6f, 0.6f);
	_Earth->SetRawData(earthRaw);

	_Mars = new Planet(4.6f, 0.0008f);
	D3D11Framework::ModelRaw marsRaw = D3D11Framework::ModelFactory::GetBox(0.6f, 0.6f, 0.6f);
	_Mars->SetRawData(marsRaw);

	_Jupiter = new Planet(6.8f, 0.0006f);
	D3D11Framework::ModelRaw juRaw = D3D11Framework::ModelFactory::GetBox(1.2f, 1.2f, 1.2f);
	_Jupiter->SetRawData(juRaw);

	_Saturn = new Planet(8.9f, 0.0007f);
	D3D11Framework::ModelRaw saturnRaw = D3D11Framework::ModelFactory::GetBox(1, 1, 1);
	_Saturn->SetRawData(saturnRaw);

	_Uranus = new Planet(12, 0.0004f);
	D3D11Framework::ModelRaw uraRaw = D3D11Framework::ModelFactory::GetBox(.5f, .5f, .5f);
	_Uranus->SetRawData(uraRaw);

	_Neptune = new Planet(15, 0.0002f);
	D3D11Framework::ModelRaw nepRaw = D3D11Framework::ModelFactory::GetBox(.4f, .4f, .4f);
	_Neptune->SetRawData(nepRaw);
}

bool SolarGame::InitializeModels() {
	AddModel(_Sun);
	AddModel(_Mercury);
	AddModel(_Venus);
	AddModel(_Earth);
	AddModel(_Mars);
	AddModel(_Jupiter);
	AddModel(_Saturn);
	AddModel(_Uranus);
	AddModel(_Neptune);
	if (!Engine::InitializeModels()) {
		return false;
	}

	return true;
}
