#pragma once

#include <Engine.h>
#include <ModelFactory.h>
#include <Graphics/Model.h>
#include <Graphics/Vertex.h>
#include "Planet.h"
#include "Sun.h"

class SolarGame : public D3D11Framework::Engine {
public:
	SolarGame();
	bool InitializeModels();

private:
	Sun * _Sun;
	Planet * _Mercury;
	Planet* _Venus;
	Planet* _Earth;
	Planet* _Mars;
	Planet* _Jupiter;
	Planet* _Saturn;
	Planet* _Uranus;
	Planet* _Neptune;
};