#include "Planet.h"
#include "Log.h"

XMMATRIX                gWorld2;

Planet::Planet() {
	// Initialize the world matrix
	gWorld2 = XMMatrixIdentity();
}

void Planet::Draw(const XMMATRIX& viewProjectionMatrix) {
	this->_Time += this->_Speed;

	XMMATRIX mSpin = XMMatrixRotationZ(-this->_Time * 5);
	XMMATRIX mOrbit = XMMatrixRotationY(-this->_Time * 2.0f);
	XMMATRIX mTranslate = XMMatrixTranslation(-this->_DistanceFromSun, 0.0f, 1.f);
	XMMATRIX mScale = XMMatrixScaling(1.f, 1.f, 1.f);

	gWorld2 = mScale * mSpin * mTranslate * mOrbit;

	this->_VertexShader->_Data._Matrix = gWorld2 * _WorldMatrix * viewProjectionMatrix; //Calculate World-View-Projection Matrix
	this->_VertexShader->_Data._Matrix = XMMatrixTranspose(this->_VertexShader->_Data._Matrix);
	this->_VertexShader->ApplyChanges();
	this->_DeviceContext->VSSetConstantBuffers(0, 1, this->_VertexShader->GetAddressOf());

	this->_DeviceContext->IASetIndexBuffer(this->_IndexBuffer.Get(), DXGI_FORMAT::DXGI_FORMAT_R32_UINT, 0);
	UINT offset = 0;
	this->_DeviceContext->IASetVertexBuffers(0, 1, this->_VertexBuffer.GetAddressOf(), this->_VertexBuffer.StridePtr(), &offset);
	this->_DeviceContext->DrawIndexed(this->_IndexBuffer.BufferSize(), 0, 0); //Draw

	//this->PrintCurrentPos();
}
