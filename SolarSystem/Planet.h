#pragma once

#include <Graphics/Model.h>

using namespace DirectX;

using namespace D3D11Framework;

class Planet : public Model {

public:
	Planet();
	Planet(float distance, float speed) : _DistanceFromSun(distance), _Speed(speed) {};
	void Draw(const XMMATRIX& viewProjectionMatrix) override;
private:
	float _DistanceFromSun;
	float _Speed;
	float _Time = 0.f;

};

