#include "Sun.h"

XMMATRIX                g_World1;

Sun::Sun() {
	// Initialize the world matrix
	g_World1 = XMMatrixIdentity();
}

void Sun::Draw(const XMMATRIX& viewProjectionMatrix) {
	static float t = 0.0f;
	t += (float) XM_PI * 0.00125f;

	g_World1 = XMMatrixRotationY(t);

	//// 2nd Cube:  Rotate around origin
	//XMMATRIX mSpin = XMMatrixRotationZ(-t); 
	//XMMATRIX mOrbit = XMMatrixRotationY(-t * 2.0f);
	//XMMATRIX mTranslate = XMMatrixTranslation(-2.0f, 0.0f, 0.0f);
	//XMMATRIX mScale = XMMatrixScaling(1.f, 1.f, 1.f);

	//g_World1 = mScale * mSpin * mTranslate * mOrbit;

	//
	// Update variables for the second cube
	//

	//this->cb_vs_vertexshader->_Data._Matrix = XMMatrixTranspose(g_View);
	//this->cb_vs_vertexshader->_Data._Matrix = XMMatrixTranspose(g_Projection);
	this->_VertexShader->_Data._Matrix = g_World1 * viewProjectionMatrix * _WorldMatrix; //Calculate World-View-Projection Matrix
	this->_VertexShader->_Data._Matrix = XMMatrixTranspose(this->_VertexShader->_Data._Matrix);
	this->_VertexShader->ApplyChanges();
	this->_DeviceContext->VSSetConstantBuffers(0, 1, this->_VertexShader->GetAddressOf());



	this->_DeviceContext->IASetIndexBuffer(this->_IndexBuffer.Get(), DXGI_FORMAT::DXGI_FORMAT_R32_UINT, 0);
	UINT offset = 0;
	this->_DeviceContext->IASetVertexBuffers(0, 1, this->_VertexBuffer.GetAddressOf(), this->_VertexBuffer.StridePtr(), &offset);
	this->_DeviceContext->DrawIndexed(this->_IndexBuffer.BufferSize(), 0, 0); //Draw
}
